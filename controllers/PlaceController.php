<?php

namespace app\controllers;

use Yii;
use app\models\MosDataApi;

class PlaceController extends \yii\rest\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ]
            ]
        ];
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionStationsNear($latitude, $longitude, $radius = 1000)
    {
        $latitude = (float)str_replace(",", '.', $latitude);
        $longitude = (float)str_replace(",", '.', $longitude);
        $api = new MosDataApi();
        $stations = $api->getStationsNear($latitude, $longitude, $radius);
        return $stations;
    }

}
