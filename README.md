#Установка#
# Установить плагин для composer: #

### php composer.phar global require "fxp/composer-asset-plugin:^1.2.0" ###

# В корне проекта запустить команду: #
### composer install ###

# Описание API: #
##Вернуть список ближайших станций метро с вестибюлями в заданном радиусе ##

### http://localhost:8000/index.php?r=place/stations-near&longitude=<longitude>&latitude=<latitude>&radius=<radius> ###

* <longitude> - широта координаты(**float, разделитель - запятая!!!**)
* <latitude> - долгота координаты(**float, разделитель - запятая!!!**)
* <radius> - радиус поиска в метрах (целое число, необязательный, по умолчанию 1000)

Пример запроса:
**/index.php?r=place/stations-near&longitude=1&latitude=2**