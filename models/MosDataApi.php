<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;
use GuzzleHttp\Client;
use yii\helpers\Json;

/**
 * ContactForm is the model behind the contact form.
 */
class MosDataApi
{
    static $baseUrl = 'http://api.data.mos.ru';
    static $methods = [
        'stations' => 'v1/datasets/624/features'
    ];
    private $client;


    private function url($method)
    {
        if (!array_key_exists($method, self::$methods)) {
            throw new Exception('Метод не существует');
        }
        return self::$baseUrl . '/' . self::$methods[$method];
    }

    private function getClient()
    {
        return $this->client;
    }

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getStations()
    {
        $cache = Yii::$app->getCache();
        $stations = $cache->get('stations');
        if ($stations === false) {
            $stations = $this->fetchStations();
            $cache->set('stations', $stations);
        }
        return $stations;
    }


    public function getStationsNear($latPoint, $longPoint, $radius)
    {
        $stations = $this->getStations();
        $nearStations = [];
        foreach ($stations as $station) {
            if (!array_key_exists('geometry', $station) || !array_key_exists('coordinates', $station['geometry'])) {
                continue;
            }
            list($lat, $long) = $station['geometry']['coordinates'];
            $distance = $this->getDistance($latPoint, $longPoint, $lat, $long);
            if ($distance < $radius) {
                array_push($nearStations, $station);
            }
        }
        return $nearStations;
    }


    // Расстояние в метрах между двумя точками
    public static function getDistance($lat1, $lon1, $lat2, $lon2)
    {
        $lat1 *= M_PI / 180;
        $lat2 *= M_PI / 180;
        $lon1 *= M_PI / 180;
        $lon2 *= M_PI / 180;

        $d_lon = $lon1 - $lon2;

        $slat1 = sin($lat1);
        $slat2 = sin($lat2);
        $clat1 = cos($lat1);
        $clat2 = cos($lat2);
        $sdelt = sin($d_lon);
        $cdelt = cos($d_lon);

        $y = pow($clat2 * $sdelt, 2) + pow($clat1 * $slat2 - $slat1 * $clat2 * $cdelt, 2);
        $x = $slat1 * $slat2 + $clat1 * $clat2 * $cdelt;

        return atan2(sqrt($y), $x) * 6372795;
    }


    private function fetchStations()
    {
        $method = 'stations';
        $url = $this->url($method);
        $response = $this->getClient()->get($url);
        if ($response->getStatusCode() !== 200) {
            throw new Exception('Ошибка при выполнении запроса');
        }
        $stations = Json::decode($response->getBody()->getContents())['features'];
        return $stations;
    }

}
